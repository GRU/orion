MBALIGN  equ  1 << 0           
MEMINFO  equ  1 << 1           
FLAGS    equ  MBALIGN | MEMINFO
MAGIC    equ  0x1BADB002     
CHECKSUM equ -(MAGIC + FLAGS)
	
section .multiboot
align 4
	dd MAGIC
	dd FLAGS
	dd CHECKSUM			

section .text
global boot
boot:
        cli
        lgdt [gdt_pointer]

        mov eax, cr0
        or eax,0x1 ; set protected mode bit
        mov cr0, eax

        mov ax, DATA_SEG
        mov ds, ax
        mov es, ax
        mov fs, ax
        mov gs, ax
        mov ss, ax
        jmp CODE_SEG:_start

gdt_start:
        dq 0x0
gdt_code:
        dw 0xFFFF
        dw 0x0
        db 0x0
        db 10011010b
        db 11001111b
        db 0x0
gdt_data:
        dw 0xFFFF
        dw 0x0
        db 0x0
        db 10010010b
        db 11001111b
        db 0x0
gdt_end:

gdt_pointer:
        dw gdt_end - gdt_start
        dd gdt_start

CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start

times 510 - ($-$$) db 0
dw 0xaa55

section .text
global _start	
_start:
	mov esp, stack_top
	
	extern early_kernel_main
	call early_kernel_main

	extern _init
	call _init
	
	extern kernel_main
	call kernel_main
	
	cli

hang:	hlt			
	jmp hang		

global idt_flush
idt_flush:
        lidt [eax]
        ret

times 1024-($-$$) db 0
	
%macro isr_err_stub 1
isr_stub_%+%1:
	cli
	push %1
	jmp isr_wrapper
%endmacro

%macro isr_no_err_stub 1
isr_stub_%+%1:
	cli
	push 0x0
	push %1
	jmp isr_wrapper
%endmacro

%macro irq 2
global irq%1
irq%1:
	push 0x0
	push %2
	jmp irq_wrapper
%endmacro

extern isr_handler
isr_no_err_stub 0
isr_no_err_stub 1
isr_no_err_stub 2
isr_no_err_stub 3
isr_no_err_stub 4
isr_no_err_stub 5
isr_no_err_stub 6
isr_no_err_stub 7
isr_err_stub    8
isr_no_err_stub 9
isr_err_stub    10
isr_err_stub    11
isr_err_stub    12
isr_err_stub    13
isr_err_stub    14
isr_no_err_stub 15
isr_no_err_stub 16
isr_err_stub    17
isr_no_err_stub 18
isr_no_err_stub 19
isr_no_err_stub 20
isr_no_err_stub 21
isr_no_err_stub 22
isr_no_err_stub 23
isr_no_err_stub 24
isr_no_err_stub 25
isr_no_err_stub 26
isr_no_err_stub 27
isr_no_err_stub 28
isr_no_err_stub 29
isr_err_stub    30
isr_no_err_stub 31

global isr_stub_table
isr_stub_table:
%assign i 0 
%rep    32 
    dd isr_stub_%+i ; use DQ instead if targeting 64-bit
%assign i i+1 
%endrep
	
extern irq_handler
irq   0,    32
irq   1,    33
irq   2,    34
irq   3,    35
irq   4,    36
irq   5,    37
irq   6,    38
irq   7,    39
irq   8,    40
irq   9,    41
irq  10,    42
irq  11,    43
irq  12,    44
irq  13,    45
irq  14,    46
irq  15,    47	

global irq_stub_table
irq_stub_table:
%assign i 0 
%rep    15 
    dd irq%+i ; use DQ instead if targeting 64-bit
%assign i i+1 
%endrep
	
global isr_wrapper
isr_wrapper:
	pusha
        mov    ax, ds
        push   eax
        mov    ax, 0x10
        mov    ds, ax
        mov    es, ax
        mov    fs, ax
        mov    gs, ax
	
	call isr_handler
	
	pop    eax
        mov    ds, ax
        mov    es, ax
        mov    fs, ax
        mov    gs, ax
        popa
        add    esp, 0x8
        sti
	iret

global irq_wrapper
irq_wrapper:
	pusha

	mov ax, ds
	push eax

	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	call irq_handler

	pop ebx
	mov ds, bx
	mov es, bx
	mov fs, bx
	mov gs, bx

	popa
	add esp, 0x8
	iret
	
section .bss			
align 16
stack_bottom:
resb 16384 ; 16 KiB
stack_top:
