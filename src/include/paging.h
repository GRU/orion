#ifndef PAGING_H
#define PAGING_H

#include <idt.h>

#define NPAGES 4096 // FIXME
#define PAGE_SIZE 16

typedef struct page {
  struct page *next;
  int value;
} page_t;

typedef uint32_t page_addr_t;

page_t *kalloc_frame();
void kfree_frame(page_t *page);
void paging_init();
void page_fault(registers_t regs);

#endif
