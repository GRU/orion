#ifndef _ASM_H
#define _ASM_H

#include <stdint.h>

void outb(uint16_t port, uint8_t val);
int inb(int port);

#endif
