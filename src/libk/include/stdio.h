#ifndef _STDIO_H
#define _STDIO_H

#include <vga.h>
#include <stdarg.h>

int kputchar(int c);
int kputs(const char *s);

int kvsnprintf(char *restrict s, size_t n, const char *restrict format, va_list ap);
int ksnprintf(char *restrict s, size_t n, const char *restrict format, ...);
int kvsprintf(char *restrict s, const char *restrict format, va_list ap);
int ksprintf(char *restrict s, const char *restrict format, ...);
int kvprintf(const char *restrict format, va_list ap);
int kprintf(const char *restrict format, ...);

#endif
